package com.example.fix

import java.io.Serializable

class Penyakit: Serializable {
    var PenyakitID : Int = 1
    var PenyakitName : String = ""
    var Pencegahan : String = ""
    var Penjelasan : String = ""
}

class Gejala: Serializable {
    var GejalaID : Int = 1
    var GejalaName : String = ""
    var GejalaLokasi : String = ""
}

class Makanan {
    var MakananID : Int = 1
    var MakananName : String = ""
    var MakananJenis : String = ""
}

class Indikasi {
    var IDgejala : Int = 1
    var LokasiGejala : String = ""
    var IDPenyakit : Int = 1
}

