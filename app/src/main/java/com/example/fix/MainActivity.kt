package com.example.fix

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.ImageButton
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val actionBar = supportActionBar
        actionBar!!.title = "Beranda"
//        actionBar.setDisplayHomeAsUpEnabled(true)

        // intent untuk menu 1
        val menu1ActBtn = findViewById<ImageButton>(R.id.menu1)
        menu1.setBackgroundDrawable(null);
        menu1ActBtn.setOnClickListener{
            startActivity(Intent(this@MainActivity, Menu1Activity::class.java))
        }

        // intent untuk menu 2
        val menu2ActBtn = findViewById<ImageButton>(R.id.menu2)
        menu2.setBackgroundDrawable(null)
        menu2ActBtn.setOnClickListener {
            startActivity(Intent(this@MainActivity, Menu2Activity::class.java))
        }

        //intent untuk menu 3
        val menu3ActBtn = findViewById<ImageButton>(R.id.menu3)
        menu3.setBackgroundDrawable(null)
        menu3ActBtn.setOnClickListener {
            startActivity(Intent(this, Menu3Activity::class.java))
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
        finishAffinity()
    }

    override fun onResume() {
        super.onResume()
        KonsultasiActivity.arrayHasil.clear()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        val id = item?.getItemId()
        if (id == R.id.help){
            startActivity(Intent(this, HelpActivity::class.java))
        }
        return true
    }
}
