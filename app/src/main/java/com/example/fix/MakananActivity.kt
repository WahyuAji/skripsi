package com.example.fix

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button

class MakananActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_makanan)

        val btBackActionButton = findViewById<Button>(R.id.btBack)
        btBackActionButton.setOnClickListener {
            startActivity(Intent(this@MakananActivity, MainActivity::class.java))
        }
    }
}
