package com.example.fix

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import kotlinx.android.synthetic.main.activity_detail2.*

class Detail2Activity : AppCompatActivity() {

    companion object{
        var arrayList = arrayListOf<String>()
        var listArray = arrayListOf<String>()
        lateinit var dbHandler: DBHandler
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail2)

        dbHandler = DBHandler(this, null)

        val actionbar = supportActionBar
        actionbar!!.title = "Informasi Makanan"
        actionbar.setDisplayHomeAsUpEnabled(true)

        arrayList = intent.getStringArrayListExtra(arrayList.toString())
        val sayuranList = dbHandler.getDataMakananSayuran(this, arrayList)
        val separatorPenyakit= sayuranList.joinToString (separator =" ") { it -> "${it.MakananName}, "}
        sayuran2.text = separatorPenyakit

        listArray = intent.getStringArrayListExtra(listArray.toString())
        val buahList = dbHandler.getDataMakananBuah(this, listArray)
        val separatorPenyakit2 = buahList.joinToString (separator = " ") { "${it.MakananName}, " }
        buahan2.text = separatorPenyakit2

        val backHomeActionButton = findViewById<Button>(R.id.btBack)
        backHomeActionButton.setOnClickListener {
            startActivity(Intent(this@Detail2Activity, MainActivity::class.java))
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

//    override fun onCreateView(parent: View?, name: String?, context: Context?, attrs: AttributeSet?): View {
//        return super.onCreateView(parent, name, context, attrs)
//    }
}