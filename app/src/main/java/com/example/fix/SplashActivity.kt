package com.example.fix

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler

class SplashActivity : AppCompatActivity() {
    private var mDelayHandler: Handler?=null
    private val SPLASH_DELAY: Long = 3000
    internal val mRunnable: Runnable = Runnable {
        if (!isFinishing) {
            val SplashIntent = Intent(applicationContext, MainActivity::class.java)
            startActivity(SplashIntent)
            finish()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        mDelayHandler=Handler()
        mDelayHandler!!.postDelayed(mRunnable, SPLASH_DELAY)
    }

    public override fun onDestroy() {
        if (mDelayHandler!=null){
            mDelayHandler!!.removeCallbacks(mRunnable)
        }

        super.onDestroy()
    }
}
