package com.example.fix

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import android.util.Log
import android.widget.Toast
import com.example.fix.classifier.TrainData

class DBHandler(
    context: Context,
    factory: SQLiteDatabase.CursorFactory?
) :
    SQLiteOpenHelper(context, DATABASE_NAME, factory, DATABASE_VERSION) {

    companion object{
        private val DATABASE_NAME = "DBku.db"
        private val DATABASE_VERSION = 1

        val PENYAKIT_TABLE_NAME = "penyakit"
        val COLUMN_IDPENYAKIT = "IDPenyakit"
        val COLUMN_NAMA_PENYAKIT = "namaPenyakit"
        val COLUMN_PENJELASAN_PENYAKIT = "penjelasanPenyakit"
        val COLUMN_PENCEGAHAN_PENYAKIT = "pencegahanPenyakit"

        val GEJALA_TABLE_NAME = "gejala"
        val COLUMN_IDGEJALA = "IDGejala"
        val COLUMN_NAMA_GEJALA = "namaGejala"
        val COLUMN_LOKASI_GEJALA = "lokasiGejala"

        val MAKANAN_TABLE_NAME = "makanan"
        val COLUMN_IDMAKANAN = "IDMakanan"
        val COLUMN_NAMA_MAKANAN = "namaMakanan"
        val COLUMN_JENIS_MAKANAN = "jenisMakanan"

        val PENYAKIT_GEJALA_TABLE = "mengindikasikan"
        val IDGEJALA_PENYAKIT = "idtok1"
        val COLUMN_PENYAKITID1 = "penyakitID"
        val COLUMN_GEJALAID = "gejalaID"
        val COLUMN_LETAK_GEJALA = "lokasi"

        val PENYAKIT_MAKANAN_TABLE = "BukanRekomendasi"
        val IDMAKANAN_PENYAKIT = "idtok2"
        val COLUMN_PENYAKITID2 = "penyakitID"
        val COLUMN_MAKANANID = "makananID"

        var query1 : String? = null
    }

    override fun onCreate(db: SQLiteDatabase?) {

        ////////////////////////////////////////////////////////////////////////////////CREATE TABLE/////////////////////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        ///////////////////////////////////////////////////////////////////////////////PRIMARY TABLE/////////////////////////////////////////////////////////////////////////////
        val CREATE_PENYAKIT_TABLE = ("CREATE TABLE $PENYAKIT_TABLE_NAME (" +
                "$COLUMN_IDPENYAKIT INTEGER PRIMARY KEY AUTOINCREMENT, " +
                "$COLUMN_NAMA_PENYAKIT TEXT, " +
                "$COLUMN_PENJELASAN_PENYAKIT TEXT, " +
                "$COLUMN_PENCEGAHAN_PENYAKIT TEXT)")

        val CREATE_GEJALA_TABLE = ("CREATE TABLE $GEJALA_TABLE_NAME (" +
                "$COLUMN_IDGEJALA INTEGER PRIMARY KEY AUTOINCREMENT, " +
                "$COLUMN_NAMA_GEJALA TEXT, "+
                "$COLUMN_LOKASI_GEJALA TEXT)")

        val CREATE_MAKANAN_TABLE = ("CREATE TABLE $MAKANAN_TABLE_NAME (" +
                "$COLUMN_IDMAKANAN INTEGER PRIMARY KEY AUTOINCREMENT, " +
                "$COLUMN_NAMA_MAKANAN TEXT, " +
                "$COLUMN_JENIS_MAKANAN TEXT)")

        ///////////////////////////////////////////////////////////////////////////////TABEL ENTITAS LEMAH///////////////////////////////////////////////////////////////////////
        val CREATE_BUKAN_REKOMENDASI_TABLE = ("CREATE TABLE $PENYAKIT_MAKANAN_TABLE (" +
                "$IDMAKANAN_PENYAKIT INTEGER PRIMARY KEY AUTOINCREMENT, " +
                "$COLUMN_PENYAKITID1 INTEGER, " +
                "$COLUMN_MAKANANID INTEGER)")
        val CREATE_INDIKASI_TABLE = ("CREATE TABLE $PENYAKIT_GEJALA_TABLE (" +
                "$IDGEJALA_PENYAKIT INTEGER PRIMARY KEY AUTOINCREMENT, " +
                "$COLUMN_PENYAKITID2 INTEGER, " +
                "$COLUMN_LETAK_GEJALA TEXT, " +
                "$COLUMN_GEJALAID INTEGER)")

        ///////////////////////////////////////////////////////////////////INPUT DATA TO TABLE///////////////////////////////////////////////////////////////////////////////////
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        val CREATE_DATA_PENYAKIT = ("INSERT INTO $PENYAKIT_TABLE_NAME ( $COLUMN_NAMA_PENYAKIT, $COLUMN_PENJELASAN_PENYAKIT, $COLUMN_PENCEGAHAN_PENYAKIT) VALUES " +
                "('Dementia','Dementia merupakan sindrom akibat penyakit otak yang bersifat kronik progresif, ditandai dengan kemunduran fungsi kognitif multiple, termasuk daya ingat (memori), daya pikir, daya tangkap (komprehensi), kemampuan belajar, orientasi, kalkulasi, visuospasial, bahasa dan daya nilai.', 'Rutin berolahraga, menjaga pola makan sehat, menjaga berat badan, tidak merokok, tidur yang cukup.'), " +
                "('Asma', 'jenis penyakit jangka panjang atau kronis pada saluran pernapasan yang ditandai dengan peradangan dan penyempitan saluran napas yang menimbulkan sesak atau sulit bernapas.', 'Hindari rokok.'), " +
                "('Gagal Jantung', 'Gagal Jantung merupakan kondisi ketika pompa jantung melemah sehingga tidak mampu mengalirkan darah yang cukup ke seluruh tubuh', 'Mengonsumsi makanan sehat dan membatasi asupan garam, lemak, dan gula.'), " +
                "('Ginjal Kronis','Penyakit ginjal kronis merupakan kondisi saat fungsi ginjal menurun secara bertahap karena kerusakan ginjal.','Menjalani pola hidup sehat.'), " +
                "('Bronkitis Kronis', ' infeksi umum yang menyebabkan peradangan dan iritasi pada saluran udara utama paru-paru atau bronkus', 'Menghindari rokok.'), " +
                "('Diabetes Mellitus', 'kondisi kronis dan berlangsung seumur hidup yang memengaruhi kemampuan tubuh dalam menggunakan energi dari makanan yang telah dicerna.', 'Olahraga, menghindari makanan yang mengandung tinggi gula'), " +
                "('Stroke', 'Merupakan kondisi yang terjadi ketika pasokan darah ke otak terganggu atau berkurang akibat penyumbatan (stroke iskemik) atau pecagnya pembuluh darah (stroke hemoragik), sehingga otak tidak akan mendapatkan asupan oksigen dan nutrisi lalu sel-sel pada sebagian area otak akan mati.', 'Olahraga teratur, Diet makanan rendah lemak.'), " +
                "('Hipertensi', 'Hipertensi merupakan kondisi dimana tekanan darah lebih tinggi dari 140/90 milimeter merkuri (mmHg) sehingga jantung memompa darah ke seluruh tubuh.','Menjaga berat badan ideal, berolahraga secara rutin, mengurangi garam, mengurangi konsumsi alkohol, berhenti merokok, menjaga pola makan sehat rendah lemak dan kaya serat.'), " +
                "('Kolesterol Tinggi (Dislipidemia)','Dislipideia merupakan kondisi yang terjadi ketika kadar lipid (lemak) di dalam darah terlalu tinggi atau terlalu rendah.','Mengurangi konsumsi lemak jahat, olahraga rutin, mengurangi konsumsi alkohol, memperbanyak minum air putih, memperbanyak serat buah dan sayur.'), " +
                "('Asam Urat (Gout Arthritis)','Merupakan kondisi dimana kadar asam urat yang tinggi sehingga membentuk kristal dengan bentuk seperti jarum di sendi dan menyebabkan serangan gout yang sangat nyeri disertai kemerahan, bengkak dan hangat di area tersebut.','Minum banyak cairan, menghindari alkohol, membatasi asupan dagin, ikan dan unggas serta menjaga berat badan ideal.'), " +
                "('Gagal Ginjal', 'Penyakit gagal ginjal merupakan kondisi yang terjadi ketika ginjal kehilangan kemampuan untuk menyaring zat sisa dari darah dengan baik.', 'Menjaga berat badan ideal, menghilangkan kebiasaan merokok'), "+
                "('Hepatitis Kronis','Hepatitis Kronis peradangan hati yang berlangsung setidaknya selama 6 bulan.','Menggunakan vaksin hepatitis B, berhati-hati dengan penggunaan jarum, menghindari freesex, rajin mencuci tangan')")

        val CREATE_DATA_GEJALA =("INSERT INTO $GEJALA_TABLE_NAME ( $COLUMN_NAMA_GEJALA, $COLUMN_LOKASI_GEJALA ) VALUES ('Batuk', 'Dada'), ('Demam','Badan'), ('Sesak di Dada','Dada'), ('Mengi','Dada'), ('Fatigue','Badan'), ('Kaku','Badan'), ('Pusing','Kepala'), ('Nyeri Pinggang','Pinggang'), ('Susah Kencing','Perut'), ('Bengkak','Badan'), " +
                "('Sering kencing di malam hari','Perut'), ('Sering Haus','Badan'), ('Sering Lapar','Badan'), ('Gangguan Bicara','Mulut'), ('Gangguan Berbahasa','Mulut'), ('Kelumpuhan','Badan'), ('Nyeri Sendi','Badan'), ('Sebah','Perut'), ('Sakit Kepala','Kepala'), " +
                "('Kencing diserta darah','Perut'), ('Vertigo','Kepala'), ('Mual/Muntah','Perut'), ('Nyeri di Hati','Dada'), ('Hilang Nafsu Makan','Badan'), ('Mata Kuning','Mata'), ('Kencing berwarna coklat','Perut'), ('Gangguan tidur','Badan'), ('Gangguan daya ingat','Kepala'), ('Sulit menerima informasi baru','Kepala'), " +
                "('Sering Lupa','Kepala'), ('Sesak Nafas','Dada'), ('Pembengkakan di Kaki','Kaki'), ('Detak Jantung Cepat','Dada')")
        val CREATE_DATA_MAKANAN = ("INSERT INTO $MAKANAN_TABLE_NAME ( $COLUMN_NAMA_MAKANAN, $COLUMN_JENIS_MAKANAN ) VALUES ('Daun Kemangi','Sayur'), ('Daun Salam','Sayur'), ('Bayam','Sayur'), ('Brokoli','Sayur'), ('Wortel','Sayur'), ('Kubis','Sayur '), ('Mentimun','Sayur'), ('Paprika','Sayur'), ('Asparagus','Sayur'), ('Kecambah','Sayur'), ('Bawang Merah','Sayur'), ('Rumput Laut','Sayur'), ('Kedelai','Sayur'), ('Jengkol','Sayur'), ('Kacang Panjang','Sayur'), ('Kangkung','Sayur'), ('Kentang','Sayur'), ('Terong','Sayur'), ('Kembang Kol','Sayur'), ('Pare','Sayur'), ('Lobak','Sayur'), ('Sawi Hijau','Sayur'), ('Seledri','Sayur'), ('Labu','Sayur'), ('Anggur','Buah'), ('Apel','Buah'), ('Alpukat','Buah'), ('Buah Naga','Buah'), ('Durian','Buah'), ('Jambu Biji','Buah'), ('Jeruk','Buah'), ('Jeruk Nipis','Buah'), ('Kelapa','Buah'), ('Mangga','Buah'), ('Manggis','Buah'), ('Melon','Buah'), ('Nanas','Buah'), ('Nangka','Buah'), ('Pepaya','Buah'), ('Pisang','Buah'), ('Salak','Buah'), ('Semangka','Buah'), ('Sirsak','Buah'), ('Srikaya','Buah'), ('Tomat','Buah'), ('Aprikot','Buah'), ('Kiwi','Buah')")

        val CREATE_DATA_BUKAN_REKOMENDASI = ("INSERT INTO $PENYAKIT_MAKANAN_TABLE ($COLUMN_PENYAKITID1, $COLUMN_MAKANANID) VALUES ('3','20'), ('3','22'), ('3','23'), ('3','39'), ('3','30'), ('3','38'), ('7','30'), ('5','30'), ('5','39'), ('5','35'), " +
                "('5','28'), ('5','34'), ('6','20'), ('6','22'), ('6','23'), ('6','39'), ('6','30'), ('6','38'), ('10','18'), ('10','9'), " +
                "('10','25'), ('10','4'), ('10','3'), ('10','28'), ('10','42'), ('10','32'), ('10','49'), ('10','38'), ('10','26')")
        val CREATE_DATA_INDIKASI = ("INSERT INTO $PENYAKIT_GEJALA_TABLE ($COLUMN_GEJALAID, $COLUMN_LETAK_GEJALA, $COLUMN_PENYAKITID2) VALUES ('1','Dada','5'), ('2','Badan','5'), ('3','Dada','5'), ('4','Dada','5'), ('5','Badan','9'), ('6','Badan','9'), ('7','Kepala','9'), ('8','Pinggang','11'), ('9','Perut','11'), ('10','Badan','11'), " +
                "('5','Badan','6'), ('11','Perut','6'), ('12','Badan','6'), ('13','Badan','6'), ('14','Mulut','7'), ('15','Mulut','7'), ('16','Mulut','7'), ('17','Sendi','10'), ('6','Badan','10'), ('10','Sendi','10'), " +
                "('2','Badan','10'), ('5','Badan','8'), ('18','Perut','8'), ('19','Kepala','8'), ('20','Perut','4'), ('11','Perut','6'), ('21','Kepala','8'), ('1','Dada','2'), ('4','Dada','2'), ('3','Dada','2')," +
                "('22','Perut','12'), ('23','Dada','12'), ('24','Badan','12'), ('5','Badan','12'), ('25','Mata','12'), ('26','Perut','12'), ('5','Badan','4'), ('9','Perut','4'), ('27','Badan','4'), ('22','Perut','4')," +
                "('28','Kepala','1'), ('29','Kepala','1'), ('30','Kepala','1'), ('31','Dada','3'), ('5','Badan','3'), ('32','Kaki','3'), ('33','Dada','3')")

//tambah data gerd


//        Log.i("data_penyakit", CREATE_DATA_PENYAKIT)
        db?.execSQL(CREATE_PENYAKIT_TABLE)
        db?.execSQL(CREATE_GEJALA_TABLE)
        db?.execSQL(CREATE_MAKANAN_TABLE)
        db?.execSQL(CREATE_DATA_GEJALA)
        db?.execSQL(CREATE_DATA_MAKANAN)
        db?.execSQL(CREATE_DATA_PENYAKIT)
        db?.execSQL(CREATE_BUKAN_REKOMENDASI_TABLE)
        db?.execSQL(CREATE_INDIKASI_TABLE)
        db?.execSQL(CREATE_DATA_INDIKASI)
        db?.execSQL(CREATE_DATA_BUKAN_REKOMENDASI)
    }

    fun getDataPenyakit(context: Context) : ArrayList<Penyakit>{
        val qry = "SELECT * FROM $PENYAKIT_TABLE_NAME"
        val db = this.readableDatabase
        val cursor = db.rawQuery(qry, null)
        val Penyakit1 = ArrayList<Penyakit>()

        if (cursor.count == 0){
            Toast.makeText(context, "Tidak Ada Data", Toast.LENGTH_LONG).show()
        } else {
            while (cursor.moveToNext()){
                val model = Penyakit()
//                Log.i("data_penyakit", model.toString())
                model.PenyakitName = cursor.getString(cursor.getColumnIndex(COLUMN_NAMA_PENYAKIT))
                model.PenyakitID = cursor.getString(cursor.getColumnIndex(COLUMN_IDPENYAKIT)).toInt()
                model.Penjelasan = cursor.getString(cursor.getColumnIndex(COLUMN_PENJELASAN_PENYAKIT))
                Penyakit1.add(model)
            }
//            Toast.makeText(context, "${cursor.count.toString()} Records Found", Toast.LENGTH_LONG).show()
        }
        cursor.close()
        return Penyakit1
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    fun getDataGejala(context : Context) : ArrayList<Gejala> {
        val qry = "SELECT * FROM $GEJALA_TABLE_NAME"
        val db = this.readableDatabase
        val cursor = db.rawQuery(qry, null)
        val Gejala1 = ArrayList<Gejala>()

        if (cursor.count == 0){
            Toast.makeText(context, "Tidak Ada Data", Toast.LENGTH_LONG).show()
        } else {
            while (cursor.moveToNext()){
                val model = Gejala()
                model.GejalaID = cursor.getString(cursor.getColumnIndex(COLUMN_IDGEJALA)).toInt()
                model.GejalaName = cursor.getString(cursor.getColumnIndex(COLUMN_NAMA_GEJALA))
                Gejala1.add(model)
            }
//            Toast.makeText(context, "${cursor.count.toString()} Records Found", Toast.LENGTH_LONG).show()
        }
        cursor.close()
        return Gejala1
    }

//    fun getDataLokasiGejala(context: Context) : ArrayList<Gejala> {
//        val kueri = "SELECT DISTINCT $COLUMN_LOKASI_GEJALA FROM $GEJALA_TABLE_NAME ORDER BY $COLUMN_LOKASI_GEJALA"
//        val db = this.readableDatabase
//        val cursor = db.rawQuery(kueri, null)
//        val Lokasi1 = ArrayList<Gejala>()
//
//        if (cursor.count == 0){
//            Toast.makeText(context, "Tidak Ada Data", Toast.LENGTH_LONG).show()
//        } else {
//            while (cursor.moveToNext()) {
//                val model = Gejala()
//                model.GejalaLokasi = cursor.getString(cursor.getColumnIndex(COLUMN_LOKASI_GEJALA))
//                Lokasi1.add(model)
//            }
//        }
//        return Lokasi1
//    }

    fun getDataMakananSayuran(context: Context, list: ArrayList<String>) : ArrayList<Makanan>{
        for (i in list.indices){
            if (i == 0){
                query1 = "SELECT DISTINCT $COLUMN_NAMA_MAKANAN FROM $MAKANAN_TABLE_NAME INNER JOIN $PENYAKIT_MAKANAN_TABLE ON $PENYAKIT_MAKANAN_TABLE.$COLUMN_MAKANANID=$MAKANAN_TABLE_NAME.$COLUMN_IDMAKANAN INNER JOIN $PENYAKIT_TABLE_NAME ON $PENYAKIT_TABLE_NAME.$COLUMN_IDPENYAKIT=$PENYAKIT_MAKANAN_TABLE.$COLUMN_PENYAKITID2 WHERE $COLUMN_JENIS_MAKANAN='Sayur' AND $COLUMN_IDPENYAKIT!="+list[0]
            }
            if (i > 0){
                query1 = query1+ " AND $COLUMN_IDPENYAKIT!="+list[i]
            }
        }

        Log.i("cobaseh", "querynya "+query1)
//        val qry = "SELECT $COLUMN_NAMA_MAKANAN FROM $MAKANAN_TABLE_NAME WHERE $COLUMN_IDMAKANAN=$COLUMN_MAKANANID AND $COLUMN_NAMA_PENYAKIT!=${list[0].PenyakitName}"
        val db = this.readableDatabase
        val cursor = db.rawQuery(query1, null)
//        Log.i("cobaseh", "querynya "+query.toString())
        val makanan1 = ArrayList<Makanan>()

        if (cursor.count == 0){
            Toast.makeText(context, "Tidak Ada Data", Toast.LENGTH_LONG).show()
        } else {
            while (cursor.moveToNext()) {
                val model = Makanan()
                model.MakananName = cursor.getString(cursor.getColumnIndex(COLUMN_NAMA_MAKANAN))
                makanan1.add(model)
            }
//            Toast.makeText(context, "${cursor.count.toString()} Records Found", Toast.LENGTH_LONG).show()
        }
        cursor.close()
        return makanan1
    }

    fun getDataMakananBuah(context: Context, list: ArrayList<String>) : ArrayList<Makanan>{
        for (i in list.indices){
            if (i == 0){
                query1 = "SELECT DISTINCT $COLUMN_NAMA_MAKANAN FROM $MAKANAN_TABLE_NAME INNER JOIN $PENYAKIT_MAKANAN_TABLE ON $PENYAKIT_MAKANAN_TABLE.$COLUMN_MAKANANID=$MAKANAN_TABLE_NAME.$COLUMN_IDMAKANAN INNER JOIN $PENYAKIT_TABLE_NAME ON $PENYAKIT_TABLE_NAME.$COLUMN_IDPENYAKIT=$PENYAKIT_MAKANAN_TABLE.$COLUMN_PENYAKITID2 WHERE $COLUMN_JENIS_MAKANAN='Buah' AND $COLUMN_IDPENYAKIT!="+list[0]
            }
            if (i > 0){
                query1 = query1+ " AND $COLUMN_IDPENYAKIT!="+list[i]
            }
        }

        Log.i("cobaseh", "querynya "+query1)
        val db = this.readableDatabase
        val cursor = db.rawQuery(query1, null)
        val makanan1 = ArrayList<Makanan>()

        if (cursor.count == 0){
            Toast.makeText(context, "Tidak Ada Data", Toast.LENGTH_LONG).show()
        } else {
            while (cursor.moveToNext()) {
                val model = Makanan()
                model.MakananName = cursor.getString(cursor.getColumnIndex(COLUMN_NAMA_MAKANAN))
                makanan1.add(model)
            }
//            Toast.makeText(context, "${cursor.count.toString()} Records Found", Toast.LENGTH_LONG).show()
        }
        cursor.close()
        return makanan1
    }

    fun getDataDetailPenyakit(context: Context, idPenyakit: Int) : ArrayList<Penyakit>{
        val qry = "SELECT * FROM $PENYAKIT_TABLE_NAME WHERE $COLUMN_IDPENYAKIT = " + idPenyakit
        val db = this.readableDatabase
        val cursor = db.rawQuery(qry, null)
        val penyakitd1 = ArrayList<Penyakit>()

        if (cursor.count == 0){
            Toast.makeText(context, "Tidak Ada Data", Toast.LENGTH_LONG).show()
        } else {
            while (cursor.moveToNext()){
                val model = Penyakit()
                model.PenyakitName = cursor.getString(cursor.getColumnIndex(COLUMN_NAMA_PENYAKIT))
                model.PenyakitID = cursor.getString(cursor.getColumnIndex(COLUMN_IDPENYAKIT)).toInt()
                model.Penjelasan = cursor.getString(cursor.getColumnIndex(COLUMN_PENJELASAN_PENYAKIT))
                model.Pencegahan = cursor.getString(cursor.getColumnIndex(COLUMN_PENCEGAHAN_PENYAKIT))
                penyakitd1.add(model)
            }
//            Toast.makeText(context, "${cursor.count} Records Found", Toast.LENGTH_LONG).show()
        }
        cursor.close()
        return penyakitd1
    }

    fun getDataDetailMakananSayur(context: Context, idPenyakit2: Int) : ArrayList<Makanan>{
        val qry = "SELECT DISTINCT $COLUMN_NAMA_MAKANAN FROM $MAKANAN_TABLE_NAME INNER JOIN $PENYAKIT_MAKANAN_TABLE ON $PENYAKIT_MAKANAN_TABLE.$COLUMN_MAKANANID=$MAKANAN_TABLE_NAME.$COLUMN_IDMAKANAN INNER JOIN $PENYAKIT_TABLE_NAME ON $PENYAKIT_MAKANAN_TABLE.$COLUMN_PENYAKITID2!=$PENYAKIT_TABLE_NAME.$COLUMN_IDPENYAKIT WHERE $COLUMN_JENIS_MAKANAN='Sayur' AND $COLUMN_IDPENYAKIT="+ idPenyakit2
        Log.i("Query", qry.toString())
        val db = this.readableDatabase
        val cursor = db.rawQuery(qry, null)
        val makanan = ArrayList<Makanan>()

//        Log.i("querynya", qry.toString())
        if (cursor.count == 0){
            Toast.makeText(context, "Tidak ada makanan yang direkomendasikan", Toast.LENGTH_LONG).show()
        } else {
            while (cursor.moveToNext()){
                val model = Makanan()
//                model.MakananID = cursor.getString(cursor.getColumnIndex(COLUMN_MAKANANID)).toInt()
                model.MakananName = cursor.getString(cursor.getColumnIndex(COLUMN_NAMA_MAKANAN))
                makanan.add(model)
            }
        }
        cursor.close()
        return makanan
    }
    fun getDataDetailMakananBuah(context: Context, idPenyakit2: Int) : ArrayList<Makanan>{
        val qry = "SELECT DISTINCT $COLUMN_NAMA_MAKANAN FROM $MAKANAN_TABLE_NAME INNER JOIN $PENYAKIT_MAKANAN_TABLE ON $PENYAKIT_MAKANAN_TABLE.$COLUMN_MAKANANID=$MAKANAN_TABLE_NAME.$COLUMN_IDMAKANAN INNER JOIN $PENYAKIT_TABLE_NAME ON $PENYAKIT_MAKANAN_TABLE.$COLUMN_PENYAKITID2!=$PENYAKIT_TABLE_NAME.$COLUMN_IDPENYAKIT WHERE $COLUMN_JENIS_MAKANAN='Buah' AND $COLUMN_IDPENYAKIT="+ idPenyakit2
        Log.i("Query", qry.toString())
        val db = this.readableDatabase
        val cursor = db.rawQuery(qry, null)
        val makanan = ArrayList<Makanan>()
        if (cursor.count == 0){
            Toast.makeText(context, "Tidak ada makanan yang direkomendasikan", Toast.LENGTH_LONG).show()
        } else {
            while (cursor.moveToNext()){
                val model = Makanan()
                model.MakananName = cursor.getString(cursor.getColumnIndex(COLUMN_NAMA_MAKANAN))
                makanan.add(model)
            }
        }
        cursor.close()
        return makanan
    }
    fun getDataNamaGejala(context : Context, idPenyakit: Int) : ArrayList<Gejala> {
//        val qry = "SELECT DISTINCT $COLUMN_NAMA_GEJALA FROM $GEJALA_TABLE_NAME INNER JOIN $PENYAKIT_GEJALA_TABLE"
        val qry = "SELECT $COLUMN_NAMA_GEJALA FROM $GEJALA_TABLE_NAME INNER JOIN $PENYAKIT_GEJALA_TABLE ON $PENYAKIT_GEJALA_TABLE.$COLUMN_GEJALAID=$GEJALA_TABLE_NAME.$COLUMN_IDGEJALA INNER JOIN $PENYAKIT_TABLE_NAME ON $PENYAKIT_GEJALA_TABLE.$COLUMN_PENYAKITID1=$PENYAKIT_TABLE_NAME.$COLUMN_IDPENYAKIT WHERE $COLUMN_IDPENYAKIT="+ idPenyakit
        val db = this.readableDatabase
        val cursor = db.rawQuery(qry, null)
        val Gejala1 = ArrayList<Gejala>()

        if (cursor.count == 0){
            Toast.makeText(context, "Tidak Ada Data", Toast.LENGTH_LONG).show()
        } else {
            while (cursor.moveToNext()){
                val model = Gejala()
                model.GejalaName = cursor.getString(cursor.getColumnIndex(COLUMN_NAMA_GEJALA))
                Gejala1.add(model)
            }
//            Toast.makeText(context, "${cursor.count.toString()} Records Found", Toast.LENGTH_LONG).show()
        }
        cursor.close()
        return Gejala1
    }
    fun getTrainingData(context: Context): ArrayList<TrainData>{
        val qry = "SELECT * FROM $PENYAKIT_GEJALA_TABLE"
        val db = this.readableDatabase
        val cursor = db.rawQuery(qry, null)
        val indikasiPenyakit = ArrayList<TrainData>()

        if (cursor.count == 0 ){
            Toast.makeText(context,"Tidak Ada Data", Toast.LENGTH_LONG).show()
        } else {
            while (cursor.moveToNext()){
                var model = TrainData(cursor.getString(cursor.getColumnIndex(COLUMN_GEJALAID)),
                    cursor.getString(cursor.getColumnIndex(COLUMN_LETAK_GEJALA)),
                    cursor.getString(cursor.getColumnIndex(COLUMN_PENYAKITID2)))
                indikasiPenyakit.add(model)
            }
        }
        cursor.close()
        return indikasiPenyakit
    }


    fun getTestData(context: Context, selectedList: ArrayList<String>): ArrayList<TrainData>{
        val ins = selectedList.joinToString(separator = ", ")
        query1 = "SELECT * FROM $PENYAKIT_GEJALA_TABLE WHERE $COLUMN_GEJALAID IN ($ins)"
        Log.i("kuweriii", query1)
        //val qry = "SELECT * FROM $PENYAKIT_GEJALA_TABLE"
        val db = this.readableDatabase
        val cursor = db.rawQuery(query1, null)
        val indikasi1 = ArrayList<TrainData>()

        if (cursor.count == 0 ){
            Toast.makeText(context,"Tidak Ada Data", Toast.LENGTH_LONG).show()
        } else {
            while (cursor.moveToNext()){
                var model = TrainData(cursor.getString(cursor.getColumnIndex(COLUMN_GEJALAID)),
                    cursor.getString(cursor.getColumnIndex(COLUMN_LETAK_GEJALA)),
                    cursor.getString(cursor.getColumnIndex(COLUMN_PENYAKITID2)))
                indikasi1.add(model)
            }
        }
        cursor.close()
        return indikasi1
    }
}