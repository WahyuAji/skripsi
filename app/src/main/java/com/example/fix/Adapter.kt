package com.example.fix

import android.content.Context
import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.Toast
//import com.example.fix.DBHandler.Companion.COLUMN_IDPENYAKIT
//import com.example.fix.DBHandler.Companion.COLUMN_NAMA_MAKANAN
//import com.example.fix.DBHandler.Companion.COLUMN_NAMA_PENYAKIT
//import com.example.fix.DBHandler.Companion.COLUMN_PENCEGAHAN_PENYAKIT
//import com.example.fix.DBHandler.Companion.COLUMN_PENJELASAN_PENYAKIT
//import com.example.fix.DBHandler.Companion.MAKANAN_TABLE_NAME
//import com.example.fix.DBHandler.Companion.PENYAKIT_TABLE_NAME
import kotlinx.android.synthetic.main.penyakit_rv.view.*
import kotlinx.android.synthetic.main.penyakit_rv_menu3.view.*
import kotlinx.android.synthetic.main.rv_gejala.view.*


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////ADAPTER UNTUK MENU 1////////////////////////////////////////////////
class Adapter(val context: Context, val penyakit : ArrayList<Penyakit>) : RecyclerView.Adapter<Adapter.ViewHolder>() {

    class ViewHolder (itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bindItem(context: Context, pnykt: Penyakit, id: String){
            with(itemView){
                with(pnykt){
                    txtPenyakit.text = PenyakitName
                    Log.d("penjelasan",Penjelasan)
                }
                itemView.setOnClickListener{
                    // listener(penyakit)
                    val intent = Intent(context, DetailActivity::class.java)

                    intent.putExtra(DetailActivity.idPenyakit, id)
                    intent.putExtra(DetailActivity.idPenyakit2, id)
                    intent.putExtra(DetailActivity.idPenyakit3, id)
                    intent.putExtra(DetailActivity.idGejala, id)
                    context.startActivity(intent)
                }
            }
//            symptom.text = gejala.GejalaName
//            makanan1.text = makanan.MakananName
//            makanan2.text = makanan.MakananName

//            val query = "SELECT $COLUMN_PENJELASAN_PENYAKIT, $COLUMN_PENCEGAHAN_PENYAKIT FROM $PENYAKIT_TABLE_NAME WHERE $COLUMN_NAMA_PENYAKIT=$idpenyakit"

        }
    }

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): Adapter.ViewHolder {
        val hohoho = LayoutInflater.from(p0.context).inflate(R.layout.penyakit_rv, p0, false)
        return ViewHolder(hohoho)
    }

    override fun getItemCount(): Int {
        return penyakit.size
    }

    override fun onBindViewHolder(p0: ViewHolder, p1: Int) {
//        val penyakit2 : Penyakit = penyakit[p1]
//        p0.bindItem(context, penyakit[p1], gejala[p1], makanan[p1], penyakit[p1].PenyakitID.toString())
        p0.bindItem(context, penyakit[p1], penyakit[p1].PenyakitID.toString())
//        p0.penjelasan.text=penyakit[p1].Penjelasan
//        p0.txtPenyakit.text = penyakit2.PenyakitName
    }
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////ADAPTER UNTUK MENU 3///////////////////////////////////////////////////////////////////
class AdapterMenu3(val context: Context, val penyakit: ArrayList<Penyakit>) : RecyclerView.Adapter<AdapterMenu3.ViewHolder>(){

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): AdapterMenu3.ViewHolder {
        val hehehe = LayoutInflater.from(p0.context).inflate(R.layout.penyakit_rv_menu3, p0, false)
        listPenyakit.removeAll(listPenyakit)
        return ViewHolder(hehehe)
    }

    override fun getItemCount(): Int {
        return penyakit.size
    }

    override fun onBindViewHolder(p0: AdapterMenu3.ViewHolder, p1: Int) {
        val penyakit2 : Penyakit = penyakit[p1]
        p0.cbPenyakit.text = penyakit2.PenyakitName
        p0.bindItem(penyakit[p1], context)
    }

    fun getArrayListData (): ArrayList<String>{
        return listPenyakit
    }

    companion object{
        val listPenyakit = arrayListOf<String>()
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        val cbPenyakit = itemView.cbPenyakit
        var penyakit : Penyakit? = null

//        val addedListPenyakit = arrayListOf<String>()
        fun bindItem(penyakit: Penyakit, con : Context){
            this.penyakit = penyakit

            with(itemView){
                with(penyakit){
                    cbPenyakit.setOnClickListener {
//                        Toast.makeText(con, "id : " + penyakit.PenyakitID, Toast.LENGTH_SHORT).show()
                        if(cbPenyakit.isChecked){
                            listPenyakit.add(PenyakitID.toString())
                        }
                        else if(!cbPenyakit.isChecked){
                            listPenyakit.remove(PenyakitID.toString())
                        }
                    }
                }
            }
        }
    }
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////ADAPTER UNTUK MENU 2 Gejala////////////////////////////////////////////////
class AdapterMenu2(val context: Context, val gejala: ArrayList<Gejala>) : RecyclerView.Adapter<AdapterMenu2.ViewHolder>(){
    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ViewHolder {
        val huhuhu = LayoutInflater.from(p0.context).inflate(R.layout.rv_gejala, p0, false)
        listIndikasi.removeAll(listIndikasi)
        return ViewHolder(huhuhu)
    }

    override fun getItemCount(): Int {
        return gejala.size
    }

    fun getArrayListData (): ArrayList<String>{
        return listIndikasi
    }

    override fun onBindViewHolder(holder: ViewHolder, p1: Int) {
        val gejala1 : Gejala = gejala[p1]
        holder.cbGejala.text = gejala1.GejalaName
        holder.bindItem(gejala[p1], context)
        holder.cbGejala.isChecked = gejala1.GejalaName in CheckedName
        holder.cbGejala.setOnClickListener {
            if(holder.cbGejala.isChecked){
                holder.cbGejala.isChecked=true

                CheckedName.add(gejala[p1].GejalaName)
                listIndikasi.add(gejala[p1].GejalaID.toString())
//                Toast.makeText(context,"CLICKED!!!!! $listIndikasi", Toast.LENGTH_LONG).show()
            }
            else{
                holder.cbGejala.isChecked=false
                CheckedName.remove(gejala[p1].GejalaName)
                listIndikasi.remove(gejala[p1].GejalaID.toString())
//                Toast.makeText(context,"UNCLICKED!!!!! $listIndikasi", Toast.LENGTH_LONG).show()
            }
        }
    }

    private var lastChecked: CheckBox? = null
    var CheckedName = arrayListOf<String>()

    class ViewHolder (itemView: View) : RecyclerView.ViewHolder(itemView){
        val cbGejala = itemView.cbGejala1
        var gejala : Gejala? = null

        fun bindItem(gejala: Gejala, context: Context){
            this.gejala = gejala
                with(itemView){
                    with(gejala){
                        println("Disini: ${gejala.GejalaID}")
//                        cbGejala1.setOnClickListener{
//                            Toast.makeText(context, "id : " + gejala.GejalaName, Toast.LENGTH_LONG).show()
//                            if (cbGejala1.isChecked){
//                                listIndikasi.add(GejalaName)
//
//                            }
//                            else if (!cbGejala1.isChecked){
//                                listIndikasi.remove(GejalaName)
//                            }
//                        }
                    }
                }
        }
    }

    companion object{
        val listIndikasi = arrayListOf<String>()
    }
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////ADAPTER UNTUK MENU 2 LETAK GEJALA//////////////////////////////////////////
//class AdapterMenu2Lokasi(val context: Context, val letak : ArrayList<Gejala>) : RecyclerView.Adapter<AdapterMenu2Lokasi.ViewHolder>(){
//    companion object{
//        val listLokasi = arrayListOf<String>()
//    }
//
//    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): AdapterMenu2Lokasi.ViewHolder {
//        val hihihi = LayoutInflater.from(p0.context).inflate(R.layout.rv_gejala, p0, false)
//        listLokasi.removeAll(listLokasi)
//        return ViewHolder(hihihi)
//    }
//
//    fun getArrayListDataLokasi (): ArrayList<String>{
//        return listLokasi
//    }
//
//    override fun getItemCount(): Int {
//        return letak.size
//    }
//
//    private var lastChecked: RadioButton? = null
//
//    override fun onBindViewHolder(holder: ViewHolder, p1: Int) {
//        val lokasi1 : Gejala = letak[p1]
//        holder.rbLokasi.text = lokasi1.GejalaLokasi
//        holder.bindItem(letak[p1], context)
//
//        holder.rbLokasi.setOnClickListener {
//            if (lastChecked==null){
//                lastChecked=holder.rbLokasi
//            } else if (lastChecked==holder.rbLokasi){
//                Toast.makeText(context, "kan sudah dipilih", Toast.LENGTH_LONG).show()
//                holder.rbLokasi.isChecked=true
//            } else {
//                lastChecked!!.isChecked=false
//                lastChecked=holder.rbLokasi
//            }
//        }
//        holder.rbLokasi.isChecked=false
//    }
//
//    class ViewHolder (itemView: View) : RecyclerView.ViewHolder(itemView) {
//        val rbLokasi = itemView.cbGejala1
//        var lokasi : Gejala? = null
//
//        fun bindItem(lokasi: Gejala, context: Context){
//            this.lokasi = lokasi
//            with(itemView){
//                with(lokasi){
//                    cbGejala1.setOnClickListener {
//                        Toast.makeText(context, "id : ", Toast.LENGTH_LONG).show()
//                        if (cbGejala1.isChecked){
//                            listLokasi.add(GejalaLokasi)
//                        } else if (!cbGejala1.isChecked){
//                            listLokasi.remove(GejalaLokasi)
//                        }
//                    }
//                }
//            }
//        }
//    }
//
//}