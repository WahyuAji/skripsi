package com.example.fix.classifier

import android.util.Log
import com.example.fix.Gejala
import com.example.fix.Penyakit
import java.io.Serializable
import kotlin.math.pow

open class Classifier(val symtomps: List<Gejala> = arrayListOf(), val diseases: List<Penyakit> = arrayListOf())
    : Serializable {

    val dataMaps: HashMap<String, (TrainData) -> Any> = hashMapOf (
        "gejalaId" to { x -> x.symptom },
        "lokasiGejala" to { x -> x.part },
        "penyakit" to { x -> x.disease }
    )

    fun uniqueVals(rows: List<TrainData>, key: String): Set<Any?> {
        return rows.map { dataMaps[key]?.invoke(it) }.toSet()
    }

    fun predict(trainData: List<TrainData>, testData: List<TrainData>): HashMap<Int, ArrayList<Int>> {
        val myTree = buildTree(trainData)
        printTree(myTree)

        val predictions = arrayListOf<Map<String, Int>>()
        val maps = hashMapOf<Int, ArrayList<Int>>()

        Log.d("Tree Prediction", "Test Data : $testData")
        for (row in testData) {
            Log.d("Tree Prediction", "row : $row")

            val prediction = classify(row, myTree)
//            Log.d("prediktion", prediction.toString())
            predictions.add(prediction)

            Log.d("Tree Prediction", "Actual: ${row.disease} Prediction: ${printLeaf(prediction)}\n\n")

            for (pr in prediction) {
                val key = pr.key.toInt()
                if(!maps.containsKey(key))
                    maps[key] = arrayListOf()

                val symptom = row.symptom.toInt()
                if(maps[key]?.contains(symptom)!!)
                    continue

                maps[key]!!.add(symptom)
            }
        }
//        Log.d("mapsnya", maps.toString())
        return maps

    }

    fun buildTree(rows: List<TrainData>): Node {
        val (gain, question) = findBestSplit(rows)
        //Log.i("DebugTest", "Gain: $gain | Question: $question")
        if (gain == 0.0 || question == null){
            return Leaf(rows, this)
        }

        val (trueRows, falseRows) = partition(rows, question)

        val trueBranch = buildTree(trueRows)
        val falseBranch = buildTree(falseRows)
        return DecisionNode(question, trueBranch, falseBranch)
    }

    fun partition(rows: List<TrainData>, question: Question): Pair<List<TrainData>, List<TrainData>>{
        val trueRows = arrayListOf<TrainData>()
        val falseRows = arrayListOf<TrainData>()

        for (row in rows){
//            Log.e("isiRow", row.toString())
            (if (question.match(row)) trueRows else falseRows).add(row)
        }
        return Pair(trueRows, falseRows)
    }

    fun findBestSplit(rows: List<TrainData>): Pair<Double, Question?> {
        var bestGain = 0.0
        var bestQuestion: Question? = null
        val currentCertainty = gini(rows)

        val maps = dataMaps
        for (key in maps.keys) {
            if(key == "penyakit")
                continue

            val values = uniqueVals(rows, key)
            val strArr = values.joinToString(", ") { x -> "'" + x.toString() + "'" }
            Log.i("Train Models","{ $strArr }")

            for (value in values) {
//                Log.e("isiMaps","key : $key --> value : $value")
                val question = Question(key, value.toString())
                val (trueRows, falseRows) = partition(rows, question)

                if (trueRows.isEmpty() || falseRows.isEmpty())
                    continue

                val gain = infoGain(trueRows, falseRows, currentCertainty)

                Log.i("DebugTest", "Gain: $gain | Question: $question")
                if (gain >= bestGain){
                    bestGain = gain
                    bestQuestion = question
                }
            }
        }
        return Pair(bestGain, bestQuestion)
    }

    fun gini(rows: List<TrainData>): Double {
        val counts = classCount(rows)
        var impurity = 1.0

        for (entry in counts)
            impurity -= (entry.value / rows.size.toDouble()).pow(2.0)

        return impurity
    }

    fun classCount(rows: List<TrainData>): Map<String, Int> {
        val map = mutableMapOf<String, Int>()

        for (row in rows) {
            val label = row.disease
            map[label] = map[label]?.plus(1) ?: 1
        }
        return map.toMap()
    }

    fun infoGain(left: List<TrainData>, right: List<TrainData>, currentUncertainty: Double): Double {
        val p = left.size / (left.size + right.size).toFloat()
        return currentUncertainty - p * gini(left) - (1 - p) * gini(right)
    }

    fun printLeaf(counts: Map<String,Int>): String {
        //Log.d("HasilCount", counts.values.sum().toString())
        val total:Float = counts.values.sum() * 1f
        //Log.d("HasilTotal", total.toString())

        val probs = mutableMapOf<String, Float>()
        var rt = ""
        for (lbl in counts) {
            val value =  (lbl.value / total) * 100
            //Log.d("HasilValue", value.toString())
            if(!value.isNaN())
                probs[lbl.key] = value
            else
                probs[lbl.key] = 0f

            if(rt.isNotEmpty())
               rt += ", "

            rt += "'${lbl.key}': '${probs[lbl.key]}%'"
        }
        return rt
    }

    fun classify(row: TrainData, node: Node): Map<String, Int>{
        if (node is Leaf)
            return node.predictions

        val decision = node as DecisionNode
        return if (decision.question.match(row))
            classify(row, decision.trueBranch)
        else
            classify(row, decision.falseBranch)
    }

    fun printTree(node: Node, spacing : String = ""){
        if(node is Leaf) {
//            print("$spacing Predict ${node.predictions}")
            Log.i("abc", "$spacing Predict ${node.predictions}")
            return
        }

        node as DecisionNode
        Log.i("abc", "$spacing ${node.question}")
//        print("$spacing ${node.question}")
//        print(spacing, + (node.question.toString()))

        Log.i("abc", "$spacing --> True: ")
//        print ("$spacing --> True: ")
        printTree(node.trueBranch, "$spacing  ")

        Log.i("abc", "$spacing --> False: ")
//        print ("$spacing --> False: ")
        printTree(node.falseBranch, "$spacing  ")
    }

    fun readablePrediction(prediction: Map<Int, List<Int>>): List<String> {
        return prediction.map { x ->
            val symptoms = x.value.map { y -> symtomps.find { k -> k.GejalaID == y }?.GejalaName }
            val key = diseases.find { k -> k.PenyakitID == x.key }?.PenyakitName
            "$key: $symptoms"
        }
    }
}