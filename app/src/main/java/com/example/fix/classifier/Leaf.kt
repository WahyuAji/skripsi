package com.example.fix.classifier

data class Leaf (
    val rows: List<TrainData>,
    val classifier: Classifier
): Node {

    val predictions: Map<String, Int> = classifier.classCount(rows)

}