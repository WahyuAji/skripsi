package com.example.fix.classifier

data class DecisionNode (
    val question: Question,
    val trueBranch: Node,
    val falseBranch: Node
) : Node
