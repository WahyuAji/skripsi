package com.example.fix.classifier

import android.util.Log

class Question(val key: String, val value: String) {

    fun match(obj: TrainData): Boolean {
        val maps: HashMap<String, (TrainData) -> Any> = hashMapOf (
            "gejalaId" to { x -> x.symptom },
            "lokasiGejala" to { x -> x.part },
            "penyakit" to { x -> x.disease }
        )
        val value = maps[key]?.invoke(obj)
        //Log.e("blabla","${this.value} ||||| $value")
//        return if (value is Int || value is Float)
//            value as Int >= this.value.toInt()
//        else
            return value  == this.value
    }

    override fun toString(): String {
        val condition = "=="
        val header: HashMap<String, String> = hashMapOf (
            "gejalaId" to "Gejala",
            "lokasiGejala" to "Lokasi",
            "penyakit" to "label"
        )
        return "Apakah ${header[this.key]} $condition ${this.value}"
    }

}
