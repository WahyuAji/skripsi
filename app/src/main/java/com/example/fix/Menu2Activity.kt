package com.example.fix

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.widget.Button
import android.widget.Toast
import com.example.fix.KonsultasiActivity.Companion.arrayHasil
import com.example.fix.classifier.Classifier

class Menu2Activity : AppCompatActivity() {

    companion object{
        lateinit var dbHandler: DBHandler
        lateinit var classifier: Classifier
    }

    lateinit var adapter2:AdapterMenu2

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_menu2)
        Log.i("Gejala", "haloooowwww")
        val actionbar2 = supportActionBar
        actionbar2!!.title = "Deteksi Penyakit"
        actionbar2.setDisplayHomeAsUpEnabled(true)

        arrayHasil.add("");

        dbHandler = DBHandler(this, null)

        ViewGejala()

        val btMenu2 = findViewById<Button>(R.id.btNext2)
        btMenu2.setOnClickListener {

            val list = adapter2.getArrayListData()
            if (list.size>0){
                val menu2list = dbHandler.getTrainingData(this)
                val userSelected = dbHandler.getTestData(this, list)
                classifier = Classifier(dbHandler.getDataGejala(this), dbHandler.getDataPenyakit(this))
                val predictions = classifier.predict(menu2list, userSelected)
                val intent = Intent(this@Menu2Activity, KonsultasiActivity::class.java)
                intent.putStringArrayListExtra(KonsultasiActivity.arrayHasil.toString(), list)
                intent.putExtra("classifier", classifier)
                intent.putExtra("predictions", predictions)
                startActivity(intent)

            }
            else{
                Toast.makeText(this, "Mohon pilih gejala terlebih dahulu", Toast.LENGTH_LONG).show()
            }
            Log.i("Gejala", "Gejala "+list.toString())



        }
//        radiogroup1.setOnCheckedChangeListener(RadioGroup.OnCheckedChangeListener{
//                _, checkId -> val radio: RadioButton = findViewById(checkId)
//            Toast.makeText(applicationContext, "${radio.text} dipilih", Toast.LENGTH_LONG).show()
//        })
//
//        btNext2.setOnClickListener {
////            val id: Int = radiogroup1.checkedRadioButtonId
////            if(id!=-1){
////                val radio:RadioButton = findViewById(id)
////                Toast.makeText(applicationContext, "Cie udah milih", Toast.LENGTH_LONG).show()
////            } else {
////                Toast.makeText(applicationContext, "Pilih dulu lah bro :) ", Toast.LENGTH_LONG).show()
////            }
////
//            startActivity(Intent(this@Menu2Activity, KonsultasiActivity::class.java))
//        }

        Log.i("clicked", arrayHasil.toString())

//        fab.setOnClickListener{
//            finish()
//            startActivity(intent)
//        }
    }

    //ketika radio diklik
//    fun radio_on_click(){
//        val radio: RadioButton = findViewById(radiogroup1.checkedRadioButtonId)
//        Toast.makeText(applicationContext, "${radio.text} dipilih 12", Toast.LENGTH_LONG).show()
//    }

    //function untuk menampilkan gejela ke recyclerview
    private fun ViewGejala(){
        val gejalaList = dbHandler.getDataGejala(this)
        adapter2 = AdapterMenu2(this, gejalaList)
        val rv21 : RecyclerView = findViewById(R.id.rv21)
        rv21.layoutManager = LinearLayoutManager(this)
        rv21.adapter=adapter2
    }

//    private fun ViewLokasiGejala(){
//        val lokasiList = dbHandler.getDataLokasiGejala(this)
//        adapter2Lokasi = AdapterMenu2Lokasi(this, lokasiList)
//        val rv22 : RecyclerView = findViewById(R.id.rv22)
//        rv22.layoutManager = LinearLayoutManager(this)
//        rv22.adapter=adapter2Lokasi
//    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }


}


