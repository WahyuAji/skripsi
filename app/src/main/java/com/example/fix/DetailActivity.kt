package com.example.fix

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import com.example.fix.Menu1Activity.Companion.dbHandler
import kotlinx.android.synthetic.main.activity_detail.*

class DetailActivity : AppCompatActivity() {

    companion object{
        var idPenyakit = "idp"
        var idPenyakit2 = "adp"
        var idPenyakit3 = "sdp"
        var idGejala = "qdp"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)

//        PenjelasanPenyakit = intent.getStringExtra(PenjelasanPenyakit)
//        PencegahanPenyakit = intent.getStringExtra(PencegahanPenyakit)
//        GejalaPenyakit = intent.getStringExtra(GejalaPenyakit)
//        Buah2an = intent.getStringExtra(Buah2an)
//        Sayur2an = intent.getStringExtra(Sayur2an)
        idPenyakit = intent.getStringExtra(idPenyakit)
        val penyakitlist = dbHandler.getDataDetailPenyakit(this, idPenyakit.toInt())
//        getPenyakitDetail
        Log.i("penyakitt", penyakitlist[0].PenyakitName)
        penjelasanpenyakit.text = penyakitlist[0].Penjelasan
        pencegahan.text = penyakitlist[0].Pencegahan

        idGejala = intent.getStringExtra(idGejala)
        val listGejala = dbHandler.getDataNamaGejala(this, idGejala.toInt())
        val separatorGejala = listGejala.joinToString (separator = " ") { "${it.GejalaName}, " }
        symptom.text=separatorGejala

        idPenyakit2 = intent.getStringExtra(idPenyakit2)
        val listsayur = dbHandler.getDataDetailMakananSayur(this, idPenyakit2.toInt())
        val separator1 = listsayur.joinToString (separator = " ") { "${it.MakananName}, "}

        idPenyakit3 = intent.getStringExtra(idPenyakit3)
        val listbuah = dbHandler.getDataDetailMakananBuah(this, idPenyakit3.toInt())
        val separator2 = listbuah.joinToString (separator = " ") { "${it.MakananName}, " }
        buahan.text = separator2
        sayuran.text = separator1

        Log.i("separator gejala", separatorGejala)
        Log.i("separator buah", separator2)
        Log.i("separator sayur", separator1)

        val actionbar = supportActionBar
        actionbar!!.title = penyakitlist[0].PenyakitName
        actionbar.setDisplayHomeAsUpEnabled(true)
//        symptom.text = GejalaPenyakit
//        buahan.text = Buah2an
//        sayuran.text = Sayur2an
    }
    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

}
