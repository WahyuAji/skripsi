package com.example.fix

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.widget.Button
import android.widget.Toast
import android.widget.Toast.LENGTH_LONG

class Menu3Activity : AppCompatActivity() {

//    private val db by lazy { DBHandler() }

    companion object{
        lateinit var dbHandler: DBHandler
        lateinit var btn : Button
    }
    lateinit var adapter3:AdapterMenu3

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_menu3)

        btn = findViewById(R.id.btNext)

        val actionbar = supportActionBar
        actionbar!!.title = "Pemilihan Menu Makanan"
        actionbar.setDisplayHomeAsUpEnabled(true)

        dbHandler = DBHandler(this, null)

        ViewPenyakit()

        val btMenu3 = findViewById<Button>(R.id.btNext)
        btMenu3.setOnClickListener {

            val list = adapter3.getArrayListData()
            if (list.size>0){
            Log.i("penyakit","clicked: "+list.toString())
            val i = Intent(this@Menu3Activity, Detail2Activity::class.java)
            i.putStringArrayListExtra(Detail2Activity.arrayList.toString(), list)
            startActivity(i)}
            else{
//                Toast.makeText(this,"kosong", LENGTH_LONG).show()
            }
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    private fun ViewPenyakit() {
        val penyakitList = dbHandler.getDataPenyakit(this)
        adapter3 = AdapterMenu3(this, penyakitList)
        val rv3 : RecyclerView = findViewById(R.id.rv3)
        rv3.layoutManager = LinearLayoutManager(this)
        rv3.adapter = adapter3
    }



//    override fun onResume() {
//        ViewPenyakit()
//        super.onResume()
//    }
}
