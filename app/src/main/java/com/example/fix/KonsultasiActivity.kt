package com.example.fix

import android.content.Intent
import android.gesture.Prediction
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import com.example.fix.classifier.Classifier
import com.example.fix.classifier.TrainData
import kotlinx.android.synthetic.main.activity_detail.*
import kotlinx.android.synthetic.main.activity_konsultasi.*

class KonsultasiActivity : AppCompatActivity() {
    companion object{
        var arrayHasil : ArrayList<String> = arrayListOf<String>()
        lateinit var classifier: Classifier
        lateinit var trainData : TrainData
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_konsultasi)
        val actionbarkonsultasi = supportActionBar
        actionbarkonsultasi!!.title = "Hasil"
        actionbarkonsultasi.setDisplayHomeAsUpEnabled(true)

        val HomeActionButton = findViewById<Button>(R.id.btHome)
        HomeActionButton.setOnClickListener{
            startActivity(Intent(this@KonsultasiActivity, MainActivity::class.java))
            classifier = Classifier()
        }

        val classifier = intent.getSerializableExtra("classifier") as Classifier
        val predictions = intent.getSerializableExtra("predictions") as HashMap<Int, List<Int>>

        hasil.text = classifier.readablePrediction(predictions).joinToString("\n")
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}