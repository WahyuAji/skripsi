package com.example.fix.DecisionTree

import java.util.*
import kotlin.math.pow

class DecisionTreeClassifier<T>(internal val trainingData: List<DecisionTreeClassifierDataRow<T>>, private val predicateFunctions: List<PredicateFunction<*>>){
    private var decisionTree: PredicateNode<T> = DecisionTreeNodeBuilder<T>(decisionTreeClassifier = this).build()

    internal fun calculateGiniImpurity(rows: List<DecisionTreeClassifierDataRow<T>>): Double{

        val classificationCounts : MutableMap<T, Int> = mutableMapOf()
        val distinctClassification: List<DecisionTreeClassifierDataRow<T>> = rows.distinctBy {
            it.classification()
        }

        distinctClassification.forEach{ trainingDataRow: DecisionTreeClassifierDataRow<T>->
            classificationCounts[trainingDataRow.classification()!!] =
                rows.count() { m: DecisionTreeClassifierDataRow<T> ->
                    m.classification()!!.toString() == trainingDataRow.classification().toString()
                }
        }

        val probabilities: MutableList<Double> = mutableListOf()
        classificationCounts.forEach { (_, v) ->
            probabilities.add((v.toDouble() / rows.size).pow(2))
        }

        return 1 - probabilities.sum()
    }

    internal fun evaluatePredicate(pFunc: PredicateFunction<DecisionTreeClassifierDataRow<T>>,
                                   filteredTrainingData: List<DecisionTreeClassifierDataRow<T>>): PredicateResult<T> {

        val resolveAsTrue: MutableList<DecisionTreeClassifierDataRow<T>> = mutableListOf()
        val resolveAsFalse: MutableList<DecisionTreeClassifierDataRow<T>> = mutableListOf()
        filteredTrainingData.iterator().forEach {

            row: DecisionTreeClassifierDataRow<T> ->
            when (pFunc.function.invoke(row)) {
                true -> resolveAsTrue.add(row)
                false -> resolveAsFalse.add(row)
            }
        }
        return PredicateResult(left = resolveAsFalse.toList(), right = resolveAsTrue.toList())
    }

    internal fun calculateInformationGain(rows: List<DecisionTreeClassifierDataRow<T>>): List<Predicate<T>>{

        val predicateInformationGain: MutableList<Predicate<T>> = mutableListOf()
        val giniImpurityForAllRows: Double = this.calculateGiniImpurity(rows)
        @Suppress("UNCHECKED CAST")
        val p = predicateFunctions as List<PredicateFunction<DecisionTreeClassifierDataRow<T>>>
        p.iterator().forEach { predicateFunction: PredicateFunction<DecisionTreeClassifierDataRow<T>> ->
            val result: PredicateResult<T> = evaluatePredicate(predicateFunction, rows)
            val leftGiniImpurity: Double = this.calculateGiniImpurity(rows = result.left)
            val rightGiniImpurity: Double = this.calculateGiniImpurity(rows = result.right)
            val avgImpurity: Double = (result.left.size.toDouble() / rows.size) * leftGiniImpurity +
                    (result.right.size.toDouble() / rows.size) * rightGiniImpurity
            val informationGain: Double = giniImpurityForAllRows - avgImpurity
            predicateInformationGain.add(Predicate(predicateFunction, avgImpurity, informationGain))
        }
        return predicateInformationGain.toList()
    }

    private fun classify(node: PredicateNode<T>, partition: List<DecisionTreeClassifierDataRow<T>>): T {
        return when {
            (partition.size == 1) -> partition.first()
            (partition.size > 1) -> partition[(0..(partition.size -1)).random()]

            else -> partition[(0..(node.nodeResult!!.size -1)).random()]
        }.classification()!!
    }

    private tailrec fun evaluateWithTree(row: DecisionTreeClassifierDataRow<T>,
                                         node: PredicateNode<T>): T {
        with(node.nodeResult!!) {
            if (this.size == 1
                || this.isEmpty()
                || node.predictionFunction == null
            ) return classify(node, partition = this )
        }
        val result: Boolean = node.predictionFunction!!.function.invoke(row)
        val nextNode: PredicateNode<T> = if (!result) {
            if (node.leftNode == null) {
                return classify(node, partition = node.nodeResult)
            }
            node.leftNode!!
        } else {
            if (node.rightNode == null) {
                return classify(node, partition = node.nodeResult)
            }
            node.rightNode!!
        }
        return evaluateWithTree(row, nextNode)
    }

    fun evaluate(row: DecisionTreeClassifierDataRow<T>): T = evaluateWithTree(row, decisionTree)

    fun evaluate(rows: List<DecisionTreeClassifierDataRow<T>>): List<T> {
        val l = mutableListOf<T>()
        rows.forEach { row -> l.add(evaluateWithTree(row, decisionTree)) }
        return l.toList()
    }
}

class DecisionTreeNodeBuilder<T>(private val decisionTreeClassifier: DecisionTreeClassifier<T>){


    private tailrec fun processNode(rootNode: PredicateNode<T>,
                                    leftNodes: LinkedList<PredicateNode<T>>,
                                    evaluatePredicate: (p: PredicateFunction<DecisionTreeClassifierDataRow<T>>,
                                                        trainingData: List<DecisionTreeClassifierDataRow<T>>) -> PredicateResult<T>){
        with(rootNode.nodeResult!!) {
            if (this.size == 1 || this.map { it.classification() }.distinct().size == 1) return
        }

        val bestPredicate = this.decisionTreeClassifier
            .calculateInformationGain(rootNode.nodeResult)
            .sortedByDescending { it.informationGain }
            .first()

        if (bestPredicate.informationGain == 0.0) return

        val pFunc: PredicateFunction<DecisionTreeClassifierDataRow<T>> = bestPredicate.predicateFunction
        rootNode.result = evaluatePredicate.invoke(pFunc, rootNode.nodeResult)

        if (rootNode.result!!.left.isEmpty() || rootNode.result!!.right.isEmpty()) return
        rootNode.predictionFunction = pFunc

        val result: PredicateResult<T> = rootNode.result!!

        if(result.right.isNotEmpty()
            || result.right.size == 1
            || result.left.map { it.classification() }.distinct().size == 1) {

            rootNode.leftNode = PredicateNode(result.left)
            rootNode.rightNode = PredicateNode(result.right)

            leftNodes.push(rootNode.leftNode)
            processNode(rootNode.rightNode!!, leftNodes, evaluatePredicate)
        }
    }

    internal fun build(): PredicateNode<T>{

        val leftNodes: LinkedList<PredicateNode<T>> = LinkedList()
        val rootNode: PredicateNode<T> = PredicateNode(nodeResult = this.decisionTreeClassifier.trainingData)
        processNode(rootNode, leftNodes, decisionTreeClassifier::evaluatePredicate)

        while (leftNodes.size > 0){
            processNode(leftNodes.poll(), leftNodes, decisionTreeClassifier::evaluatePredicate)
        }

        return rootNode
    }
}

abstract class DecisionTreeClassifierDataRow<T> {
    open fun classification(): T? {
        return null
    }
}