package com.example.fix.DecisionTree

data class Predicate<T>(val predicateFunction: PredicateFunction<DecisionTreeClassifierDataRow<T>>, val avgImpurity: Double, val informationGain: Double)

data class PredicateFunction<T: DecisionTreeClassifierDataRow<*>>(val label: String, val function: (row: T) -> Boolean)

data class PredicateResult<T>(val left: List<DecisionTreeClassifierDataRow<T>>,
                              val right: List<DecisionTreeClassifierDataRow<T>>)

data class PredicateNode<T>(val nodeResult: List<DecisionTreeClassifierDataRow<T>>?,
                             var predictionFunction: PredicateFunction<DecisionTreeClassifierDataRow<T>>?=null,
                             var result: PredicateResult<T>? = null,
                             var leftNode: PredicateNode<T>? = null,
                             var rightNode: PredicateNode<T>? = null)
