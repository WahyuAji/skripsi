package com.example.fix

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.widget.LinearLayout

class Menu1Activity : AppCompatActivity() {

    companion object{
        lateinit var dbHandler: DBHandler
    }

//    lateinit var penyakit : ArrayList<Penyakit>
//    lateinit var gejala: ArrayList<Gejala>
//    lateinit var makanan: ArrayList<Makanan>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_menu1)

        val actionbar = supportActionBar
        actionbar!!.title = "Info Penyakit"
        actionbar.setDisplayHomeAsUpEnabled(true)

        dbHandler = DBHandler(this, null)

        ViewPenyakit()

    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    private fun ViewPenyakit() {
        val penyakitlist = dbHandler.getDataPenyakit(this)
        val adapter = Adapter(this, penyakitlist)
        val rv : RecyclerView = findViewById(R.id.rv)
        rv.layoutManager = LinearLayoutManager(this, LinearLayout.VERTICAL, false)
        rv.adapter = adapter
    }

    override fun onResume() {
        ViewPenyakit()
        super.onResume()
    }
}
